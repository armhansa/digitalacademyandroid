package com.armhansa.firstdayandroid.fragment

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.armhansa.firstdayandroid.R
import com.armhansa.firstdayandroid.adaptor.ToDoItemClickedListener
import com.armhansa.firstdayandroid.adaptor.ToDoListAdapter
import com.armhansa.firstdayandroid.model.ToDoModel
import com.armhansa.firstdayandroid.ui.DetailActivity
import kotlinx.android.synthetic.main.fragment_todo.*

class ToDoFragment(val view: Context) : Fragment() {
    private lateinit var toDoAdapter: ToDoListAdapter

    companion object {
        private const val TAG: String = "ToDoFragment"
        fun newInstance(context: Context): ToDoFragment =
            ToDoFragment(context)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_todo, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setView()
    }

    private fun setView() {
        val listener = object : ToDoItemClickedListener {
            override fun onItemClick(taskName: String) {
                Log.d(TAG, "Item Clicked")
                DetailActivity.startActivity(view, ToDoModel(false, taskName))
            }
        }

        toDoAdapter = ToDoListAdapter(listener)
        rvTodo.adapter = toDoAdapter
        rvTodo.layoutManager = LinearLayoutManager(view)
        rvTodo.itemAnimator = DefaultItemAnimator()

        btnAdd.setOnClickListener {
            Log.d(TAG, "Clicked!")
            todoEditText.text?.let {
                if (it.isNotEmpty()) {
                    val toDoModel = getAddModel(it.toString())
                    toDoAdapter.addListTask(toDoModel)
                    todoEditText.text = null
                }
            }
        }
    }

    private fun getAddModel(name: String) = ToDoModel(false, name)
}
