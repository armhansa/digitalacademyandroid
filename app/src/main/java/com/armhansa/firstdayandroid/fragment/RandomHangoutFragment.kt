package com.armhansa.firstdayandroid.fragment

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.armhansa.firstdayandroid.R
import kotlinx.android.synthetic.main.fragment_random_hangout.*
import kotlin.random.Random

class RandomHangoutFragment : Fragment() {

    private var isBlack: Boolean = false
    private var countDown: Int = 10

    companion object {
        fun newInstance(): RandomHangoutFragment =
            RandomHangoutFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_random_hangout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setView()
    }

    private fun setView() {
        randomBtn.setOnClickListener {
            if (countDown > 0) {
                if (isBlack) {
                    randomBtn.setBackgroundColor(Color.WHITE)
                    randomBtn.setTextColor(Color.BLACK)
                } else {
                    randomBtn.setBackgroundColor(Color.BLACK)
                    randomBtn.setTextColor(Color.WHITE)
                }
                isBlack = !isBlack

                if (randomAns()) {
                    ans.text = "ไป"
                    ans.setTextColor(Color.GREEN)
                } else {
                    ans.text = "ไม่ไป"
                    ans.setTextColor(Color.RED)
                }
                val displayText = "RANDOM (${--countDown})"
                randomBtn.text = displayText
            }
        }
    }

    private fun randomAns(): Boolean {
        return Random.nextInt(1, 100) <= 75
    }
}
