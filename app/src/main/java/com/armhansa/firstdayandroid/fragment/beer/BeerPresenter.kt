package com.armhansa.firstdayandroid.fragment.beer

import com.armhansa.firstdayandroid.entity.BeerModel
import com.armhansa.firstdayandroid.service.BeerManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BeerPresenter(val view: BeerInterface) {

    fun getBeerApi() {
        BeerManager().createService().getRandomBeer().enqueue(object: Callback<List<BeerModel>> {
            override fun onFailure(call: Call<List<BeerModel>>, t: Throwable) {
                println("armhansa > Fail to get API")
            }

            override fun onResponse(call: Call<List<BeerModel>>, response: Response<List<BeerModel>>) {
                response.body()?.apply {
                    if (this.isNotEmpty()) {
                        view.setBeer(this[0])
                    }
                }
            }
        })
    }
}
