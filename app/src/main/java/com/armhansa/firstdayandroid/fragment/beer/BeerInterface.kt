package com.armhansa.firstdayandroid.fragment.beer

import com.armhansa.firstdayandroid.entity.BeerModel

interface BeerInterface {
    fun setBeer(beerItem: BeerModel)
}