package com.armhansa.firstdayandroid.fragment.beer


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.armhansa.firstdayandroid.R
import com.armhansa.firstdayandroid.entity.BeerModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_beer.*

class BeerFragment : Fragment(), BeerInterface {

    private val presenter = BeerPresenter(this)

    companion object {
        fun newInstance(): BeerFragment =
            BeerFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_beer, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.getBeerApi()
        setView()
    }

    private fun setView() {
        randomBeerBtn.setOnClickListener {
            presenter.getBeerApi()
        }
    }

    override fun setBeer(beerItem: BeerModel) {
        nameBeer.text = beerItem.name
        var temp = "ABV: ${beerItem.abv}%"
        abvBeer.text = temp
        Picasso.with(context)
            .load(beerItem.imageUrl)
            .placeholder(R.drawable.ic_launcher_foreground)
            .into(beerImg)
        beerItem.imageUrl
        temp = "       ${beerItem.description}"
        descBeer.text = temp
    }

}
