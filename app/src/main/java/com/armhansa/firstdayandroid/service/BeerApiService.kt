package com.armhansa.firstdayandroid.service

import com.armhansa.firstdayandroid.entity.BeerModel
import retrofit2.Call
import retrofit2.http.GET

interface BeerApiService {
    @GET("v2/beers/random")
    fun getRandomBeer(): Call<List<BeerModel>>
}
