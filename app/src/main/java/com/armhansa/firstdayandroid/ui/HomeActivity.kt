package com.armhansa.firstdayandroid.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.armhansa.firstdayandroid.R
import com.armhansa.firstdayandroid.data.FragmentModel
import com.armhansa.firstdayandroid.fragment.beer.BeerFragment
import com.armhansa.firstdayandroid.adaptor.SectionsPagerAdapter
import com.armhansa.firstdayandroid.fragment.RandomHangoutFragment
import com.armhansa.firstdayandroid.fragment.ToDoFragment
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        setView()
    }

    private fun setView() {

        val fmList = listOf(
            FragmentModel("BEER", BeerFragment.newInstance()),
            FragmentModel("PARTY", RandomHangoutFragment.newInstance()),
            FragmentModel("TODO", ToDoFragment.newInstance(this))
        )

        val sectionsPagerAdapter =
            SectionsPagerAdapter(fmList, supportFragmentManager)
        viewPager.adapter = sectionsPagerAdapter
        tabs.setupWithViewPager(viewPager)
    }
}