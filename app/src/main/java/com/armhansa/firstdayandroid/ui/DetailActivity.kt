package com.armhansa.firstdayandroid.ui

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.armhansa.firstdayandroid.R
import com.armhansa.firstdayandroid.model.ToDoModel
import kotlinx.android.synthetic.main.activity_detail.*

class DetailActivity : AppCompatActivity() {

    companion object {
        private const val TAG = "DetailActivity"
        private const val EXTRA_KEY_TITLE = "TITLE_NAME"
        const val EXTRA_KEY_MODEL = "MODEL"

        fun startActivity(context: Context/*, titleName: String*/, toDoModel: ToDoModel) {
            context.startActivity(Intent(context, DetailActivity::class.java).also { myIntent ->
//                myIntent.putExtra(EXTRA_KEY_TITLE, titleName)
                myIntent.putExtra(EXTRA_KEY_MODEL, toDoModel)
            })
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        setView()
    }

    private fun setView() {
//        title = intent.getStringExtra(EXTRA_KEY_TITLE)
        val toDoModel: ToDoModel = intent.getParcelableExtra(EXTRA_KEY_MODEL) ?: ToDoModel(false, "")
        detailTitle.text = toDoModel.taskName

        println("armhansa > $toDoModel")

    }
}
