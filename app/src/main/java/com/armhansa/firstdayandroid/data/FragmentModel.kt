package com.armhansa.firstdayandroid.data

import androidx.fragment.app.Fragment

class FragmentModel (
    val tabName: String,
    val fragment: Fragment
)
