package com.armhansa.firstdayandroid.adaptor

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.armhansa.firstdayandroid.R
import com.armhansa.firstdayandroid.model.ToDoModel

class ToDoListAdapter(private val listener: ToDoItemClickedListener) : RecyclerView.Adapter<ToDoListViewHolder>() {

    private val toDoList: ArrayList<ToDoModel> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ToDoListViewHolder =
        ToDoListViewHolder(parent, listener)

    override fun getItemCount() = toDoList.count()

    override fun onBindViewHolder(holder: ToDoListViewHolder, position: Int) {
        holder.bind(toDoList[position])
    }

    fun addListTask(toDoModel: ToDoModel) {
        toDoList.add(toDoModel)
        notifyDataSetChanged()
    }

}

interface ToDoItemClickedListener {
    fun onItemClick(taskName: String)
}

class ToDoListViewHolder(parent: ViewGroup, private val listener: ToDoItemClickedListener) : RecyclerView.ViewHolder(
    LayoutInflater.from(parent.context).inflate(R.layout.item_todo, parent, false)
) {
    private val taskName: TextView = itemView.findViewById(R.id.task)
    private val taskCheckbox: CheckBox = itemView.findViewById(R.id.cbComplete)

    fun bind(toDoModel: ToDoModel) {
        taskName.text = toDoModel.taskName
        taskCheckbox.isChecked = toDoModel.isComplete
        itemView.setOnClickListener {
            listener.onItemClick(taskName.text.toString())
        }
        taskCheckbox.setOnClickListener { }

        itemView.visibility = if (toDoModel.isComplete) {
            View.GONE
        } else {
            View.VISIBLE
        }
    }
}
