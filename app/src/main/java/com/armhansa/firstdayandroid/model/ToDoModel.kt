package com.armhansa.firstdayandroid.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ToDoModel(
    var isComplete: Boolean,
    var taskName: String
): Parcelable
